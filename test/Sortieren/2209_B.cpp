#include <Arduino.h>

/*
  SilentStepStick TMC2208/TMC2209 Example
  Rsense: 0.11 Ohm
  Other examples/libraries can be found here:
  https://github.com/teemuatlut/TMCStepper
  https://github.com/trinamic/TMC-API
  https://github.com/manoukianv/TMC2208Pilot
  Example source code free to use.
  Further information: https://learn.watterott.com/license/
*/
int micsDel = 100;
int steps;
// Note: You also have to connect GND, 5V/VIO and VM.
//       A connection diagram can be found in the schematics.
#define EN_PIN    41 //enable (CFG6)
#define DIR_PIN   39 //direction
#define STEP_PIN  40 //step
// #define MS1       35
// #define MS2       37

void setup()
{
  //set pin modes
  pinMode(EN_PIN, OUTPUT);
  digitalWrite(EN_PIN, HIGH); //deactivate driver (LOW active)
  pinMode(DIR_PIN, OUTPUT);
  digitalWrite(DIR_PIN, LOW); //LOW or HIGH
  pinMode(STEP_PIN, OUTPUT);
  digitalWrite(STEP_PIN, LOW);
/*  pinMode(MS1, OUTPUT);
  digitalWrite(MS1, HIGH); //deactivate driver (LOW active)
  pinMode(MS2, OUTPUT);
  digitalWrite(MS2, LOW); //deactivate driver (LOW active)
*/
  digitalWrite(EN_PIN, LOW); //activate driver

micsDel =100;
steps = 16000;

}

void loop()
{


  for (int i = 0; i <= (steps - 1); i++) {    // 1600 Loops pro U   = 3200 Imp/U  3200 / 16 = 200 Vollschritt
  
 
  //make steps
  digitalWrite(STEP_PIN, HIGH);
  delayMicroseconds(micsDel);        // pauses for 100 microsecondsdelay(1);
  digitalWrite(STEP_PIN, LOW);
  delayMicroseconds(micsDel);        // pauses for 100 microsecondsdelay(1);
 
  }
 delay(20);

}