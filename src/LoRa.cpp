#include <arduino.h>
#include <SPI.h>
#include <LoRa.h>

#define ss 10
#define rst 7
#define dio0 4
#define txPower 2

// Switzerland	EU863-870   CEPT Rec. 70-03
// EU433

int counter = 0;

void setup() {
    Serial.begin(9600);
    while(!Serial)
        ;

    Serial.println("LoRa Sender");
    // LoRa.setTxPower(txPower, 2);
    LoRa.setPins(ss, rst, dio0);

    if(!LoRa.begin(433E6)) {
        Serial.println("Starting LoRa failed!");
        while(1)
            ;
    }
}

void loop() {
    Serial.print("Sending packet: ");
    Serial.println(counter);

    // send packet
    LoRa.beginPacket();
    LoRa.print("Hallo Simon ");
    LoRa.print(counter);
    LoRa.endPacket();

    counter++;

    delay(5000);
}