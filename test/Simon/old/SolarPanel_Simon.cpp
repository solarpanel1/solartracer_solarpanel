#include <Arduino.h>
#include <Wire.h>
#include "SPI.h"

// TFT
// http://www.barth-dev.de/online/rgb565-color-picker/
#include <Adafruit_GFX.h>
#include <ili9341_t3n_font_Arial.h>
#include <ili9341_t3n_font_ArialBold.h>
#include <ili9341_t3n_font_ComicSansMS.h>
#include <ili9341_t3n_font_OpenSans.h>
#include <myFonts/font_Michroma.h>
#include <myFonts/font_Michroma.c>
#include "ILI9341_t3n.h"
#define TFT_SS 6
#define TFT_RDANDWR 7
#define TFT_DC 9
#define TFT_CS 10
#define TFT_RST 8
#define CENTER ILI9341_t3n::CENTER
DMAMEM uint16_t fb1[320 * 240];
DMAMEM uint16_t fb2[320 * 240];
ILI9341_t3n tft = ILI9341_t3n(TFT_CS, TFT_DC, TFT_RST);

// LEDs
#define LED4_GN 29 // Links unten
#define LED5_RD 30 // Links oben
#define LED6_YG 31 // Rechts unten
#define LED7_BL 32 // Rechts oben
#define LED_DE 41  // DE für RS3485

// Adafruit RTC DS3231
// https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/arduino-usage
// https://github.com/adafruit/RTClib
#include "RTClib.h"
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sonntag",    "Montag",  "Dienstag", "Mittwoch",
                             "Donnerstag", "Freitag", "Samstag"};

#define HwSerial Serial4 // RS3485 Chip

void setup() {
    // Init
    Serial.begin(115200);
    HwSerial.begin(115200);
    delay(1000);

    // LED init
    pinMode(LED4_GN, OUTPUT);
    pinMode(LED5_RD, OUTPUT);
    pinMode(LED6_YG, OUTPUT);
    pinMode(LED7_BL, OUTPUT);
    pinMode(LED_DE, OUTPUT);
    digitalWrite(LED4_GN, HIGH);
    digitalWrite(LED5_RD, HIGH);
    digitalWrite(LED6_YG, HIGH);
    digitalWrite(LED7_BL, HIGH);
    digitalWrite(LED_DE, HIGH);

    // TFT init
    pinMode(TFT_RST, OUTPUT);
    pinMode(TFT_SS, OUTPUT);
    pinMode(TFT_RDANDWR, OUTPUT);
    digitalWrite(TFT_RST, HIGH);
    digitalWrite(TFT_SS, HIGH);
    digitalWrite(TFT_RDANDWR, LOW);
    uint16_t ID = tft.readID();

    // wait for serial port to connect
    while(!Serial) { ; }

    tft.begin();
    tft.setRotation(1);
    tft.setTextSize(2);

    digitalWrite(LED4_GN, LOW);
    digitalWrite(LED5_RD, LOW);
    digitalWrite(LED6_YG, LOW);
    digitalWrite(LED7_BL, LOW);
    digitalWrite(LED_DE, HIGH);

    Serial.println("TFT found ID = 0x");

    tft.setFont(Michroma_12);
    tft.setFrameBuffer(fb1);
    tft.fillScreen(0xFFE0);

    if(!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }

    if(rtc.lostPower()) {
        Serial.println("RTC lost power, let's set the time!");
        // When time needs to be set on a new device, or after a power loss, the
        // following line sets the RTC to the date & time this sketch was
        // compiled
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
        // This line sets the RTC with an explicit date & time, for example to
        // set January 21, 2014 at 3am you would call: rtc.adjust(DateTime(2014,
        // 1, 21, 3, 0, 0));
    }

    // When time needs to be re-set on a previously configured device, the
    // following line sets the RTC to the date & time this sketch was compiled
    // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));

    // LCD 20x4 Adafruit
}

int x1 = 20;
int x2 = 120;
int y = 20;

void loop() {
    tft.setTextColor(ILI9341_WHITE);
    tft.fillRectVGradient(10, 10, 260, 75, ILI9341_BLACK, ILI9341_BLACK);
    tft.fillScreen(ILI9341_BLACK);

    DateTime now = rtc.now();

    // Datum
    char DateBuffer[15];
    sprintf(DateBuffer, "%s_%02d.%02d.%04d%c", "date", now.day(), now.month(),
            now.year(), '\n');
    HwSerial.print(DateBuffer);
    HwSerial.print(DateBuffer);
    tft.setCursor(x1, y);
    tft.print("Datum");
    tft.setCursor(x2, y);
    tft.print(DateBuffer + 5);
    Serial.print(DateBuffer);

    // Zeit
    char TimeBuffer[13];
    sprintf(TimeBuffer, "%s_%02d:%02d:%02d%c", "time", now.hour(), now.minute(),
            now.second(), '\n');
    HwSerial.print(TimeBuffer);
    tft.setCursor(x1, y + 30);
    tft.print("Datum");
    tft.setCursor(x2, y + 30);
    tft.print(TimeBuffer + 5);
    Serial.print(TimeBuffer);

    // Temperatur
    char TemperatureBuffer[11];
    sprintf(TemperatureBuffer, "%s_%0.1f°C%c", "temp", rtc.getTemperature(),
            '\n');
    HwSerial.print(TemperatureBuffer);
    Serial.print(TemperatureBuffer);
}