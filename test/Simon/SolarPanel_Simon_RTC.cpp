#include <Arduino.h>
#include <Wire.h>
#include "SPI.h"
#include <Time.h>

// LEDs.
#define LED4_GN 29   // Links unten
#define LED5_RD 30   // Links oben
#define LED6_YG 31   // Rechts unten
#define LED7_BL 32   // Rechts oben
#define DE_RS3485 41 // DE für RS3485

// Adafruit RTC DS3231
// https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/arduino-usage
// https://github.com/adafruit/RTClib

#include "RTClib.h"
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sonntag",    "Montag",  "Dienstag", "Mittwoch",
                             "Donnerstag", "Freitag", "Samstag"};

#define HwSerial Serial4 // RS3485 Chip

void setup() {
    // Init
    Serial.begin(115200);
    HwSerial.begin(115200);
    delay(1000);

    // LED init
    pinMode(LED4_GN, OUTPUT);
    pinMode(LED5_RD, OUTPUT);
    pinMode(LED6_YG, OUTPUT);
    pinMode(LED7_BL, OUTPUT);
    pinMode(DE_RS3485, OUTPUT);
    digitalWrite(LED4_GN, HIGH);
    digitalWrite(LED5_RD, HIGH);
    digitalWrite(LED6_YG, HIGH);
    digitalWrite(LED7_BL, HIGH);
    digitalWrite(DE_RS3485, HIGH);
    while(!Serial && millis() < 8000) { ; }
    digitalWrite(LED4_GN, LOW);
    digitalWrite(LED5_RD, LOW);
    digitalWrite(LED6_YG, LOW);
    digitalWrite(LED7_BL, LOW);

    delay(2000);

    Serial.println("Starte RTC Uhr");
    if(!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }

    if(rtc.lostPower()) {
        Serial.println("RTC lost power, let's set the time!");
        // When time needs to be set on a new device, or after a power loss, the
        // following line sets the RTC to the date & time this sketch was
        // compiled
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
        // This line sets the RTC with an explicit date & time, for example to
        // set January 21, 2014 at 3am you would call:
        // rtc.adjust(DateTime(2021, 8, 30, 0, 3, 0));
    }

    // When time needs to be re-set on a previously configured device, the
    // following line sets the RTC to the date & time this sketch was compiled
    // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2021, 9, 3, 23, 18, 0));
}

int x1 = 20;
int x2 = 120;
int y = 20;

void loop() {
    DateTime now = rtc.now();

    // Datum
    char DateBuffer[15];
    sprintf(DateBuffer, "%s_%02d.%02d.%04d%c", "date", now.day(), now.month(),
            now.year(), '\n');
    HwSerial.print(DateBuffer);
    HwSerial.print(DateBuffer);
    Serial.print(DateBuffer);

    // Zeit
    char TimeBuffer[13];
    sprintf(TimeBuffer, "%s_%02d:%02d:%02d%c", "time", now.hour(), now.minute(),
            now.second(), '\n');
    HwSerial.print(TimeBuffer);
    Serial.print(TimeBuffer);

    // Temperatur
    char TemperatureBuffer[11];
    sprintf(TemperatureBuffer, "%s_%0.1f°C%c", "temp", rtc.getTemperature(),
            '\n');
    HwSerial.print(TemperatureBuffer);
    Serial.print(TemperatureBuffer);
}