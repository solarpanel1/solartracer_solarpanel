/// Alle Snippets zusammengefügt Teensy
#include <Arduino.h>
// Touchscreen
#include <Adafruit_GFX.h>
// #include <myFonts/font_Michroma.h>
// #include <ili9341_t3n_font2font_Michroma.h>
// #include "font_Arial.h"
// #include "font_ArialBold.h"
// #include <Fonts/Org_01.h>
// #include <Fonts/FreeSerif9pt7b.h>
// #include <fonts/FreeSans12pt7b.h>
// #include <Fonts/FreeMono9pt7b.h>
// #include <Fonts/FreeSans9pt7b.h>
// #include <ILI9341_fonts.h>
#include <Wire.h>
#include <ili9341_t3n_font_Arial.h>
#include <ili9341_t3n_font_ArialBold.h>
#include <ili9341_t3n_font_ComicSansMS.h>
#include <ili9341_t3n_font_OpenSans.h>
#include <myFonts/font_Michroma.h>
#include <myFonts/font_Michroma.c>
#include "ILI9341_t3n.h"
#include "SPI.h"

// #include <MCUFRIEND_kbv.h>
// MCUFRIEND_kbv tft;
//#define SERIAL_PORT Serial7         //  HardwareSerial port

#define TFT_SS 6
#define TFT_RDANDWR 7
#define TFT_DC 9
#define TFT_CS 10
#define TFT_RST 8

#define CENTER ILI9341_t3n::CENTER
DMAMEM uint16_t fb1[320 * 240];
DMAMEM uint16_t fb2[320 * 240];

ILI9341_t3n tft = ILI9341_t3n(TFT_CS, TFT_DC, TFT_RST);

// RTC
#include "RTClib.h"
RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sonntag",    "Montag",  "Dienstag", "Mittwoch",
                             "Donnerstag", "Freitag", "Samstag"};

void waitUserInput() {
    Serial.println("Press any key to continue");
    while(Serial.read() == -1) {};
    while(Serial.read() != -1) {};
    Serial.println("Continued");
}

void setup() {

    pinMode(TFT_RST, OUTPUT);
    pinMode(TFT_SS, OUTPUT);
    pinMode(TFT_RDANDWR, OUTPUT);

    digitalWrite(TFT_RST, HIGH);
    digitalWrite(TFT_SS, HIGH);
    digitalWrite(TFT_RDANDWR, LOW);

    Serial.begin(115200);
    Serial7.begin(115200);
    Serial.println("Serial Begin ");

    while(!Serial) {};
    if(!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }
    Serial.println("Started");

    if(rtc.lostPower()) {
        Serial.println("RTC lost power, let's set the time!");
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
    // rtc.adjust(DateTime(F("Jul 27 2021"),F("23:32:01")));
    // Serial.println("Zeit gesetzt");
    tft.begin();
    tft.setRotation(1);
    tft.setTextSize(2);

    // while(Serial.available() == 0) {}
    // uint16_t ID = tft.ID();

    Serial.println("Example: Font_simple");
    Serial.print("found ID = 0x");
    // Serial.println(ID,HEX);
    // if (ID == 0xD303) ID = 0x9481;

    // NOTE Schriftart
    // tft.setFont(OpenSans20);
    // tft.setFont(ComicSansMS_20);
    // tft.setFont(&FreeSans9pt7b); // Für GFX Library fonts. Sind aber stark
    // verpixelt
    tft.setFont(Michroma_12);
    tft.useFrameBuffer(fb1);
    tft.setFrameBuffer(fb1);
    // http://www.barth-dev.de/online/rgb565-color-picker/
    tft.fillScreen(0xFFE0);
}

int x1 = 20;
int x2 = 120;
int y = 20;
int lenDatabuffer;
void loop() {

    // tft.setTextColor(0x1C40, 0x39E7);
    // tft.setTextColor(ILI9341_LIGHTGREY, ILI9341_DARKGREY);
    tft.setTextColor(ILI9341_WHITE);
    tft.fillRectVGradient(10, 10, 260, 75, ILI9341_BLACK, ILI9341_DARKGREY);
    tft.fillScreen(ILI9341_DARKGREY);

    DateTime now = rtc.now();

    // Datum
    char dateBuffer[20];

    sprintf(dateBuffer, "%02u:%02u:%04u", now.day(), now.month(), now.year());

    tft.setCursor(x1, y);
    tft.print("Datum");
    tft.setCursor(x2, y);
    tft.print(dateBuffer);
    lenDatabuffer = 10;
    Serial7.print("za");
    Serial7.print(lenDatabuffer);
    Serial7.print(dateBuffer);
    Serial7.print("x");
    // S erial7.print('\n');
    delay(500);
    Serial.println(dateBuffer);

    delay(5000);

    // Zeit
    /**/
    sprintf(dateBuffer, "%02u:%02u:%02u", now.hour(), now.minute(),
            now.second());
    tft.setCursor(x1, y + 20);
    tft.print("Zeit");
    tft.setCursor(x2, y + 20);
    tft.print(dateBuffer);
    lenDatabuffer = 8;
    Serial7.print("zb-");
    Serial7.print(lenDatabuffer);
    Serial7.print(dateBuffer);
    Serial7.print("oox");
    Serial7.print('\n');
    // Serial7.println();
    delay(500);
    Serial.print(dateBuffer);
    Serial.println("mm");
    delay(4000);
    /**/
    // Temperatur start
    tft.setCursor(x1, y + 40);
    tft.print("Temp.");
    tft.setCursor(x2, y + 40);
    tft.print(rtc.getTemperature());
    tft.print((char)247);
    tft.setFont(Michroma_9);
    // Temp Grad verkleinern
    tft.setCursor(x2 + 73, y + 35);
    tft.print("o");
    tft.setFont(Michroma_12);
    // Temp Celsius schreiben
    tft.setCursor(x2 + 83, y + 40);
    tft.print("C");
    tft.updateScreenAsync();
    tft.waitUpdateAsyncComplete();
    // tft.drawRect(10, 10, 250, 75, ILI9341_BLUE); // x, y, w, h, color
    // tft.drawLine(105, y, 105, y + 40, ILI9341_WHITE); // x0, y0, x1, y1,
    // color
    tft.drawLine(105, 20, 105, 72, ILI9341_WHITE); // x0, y0, x1, y1, color
}