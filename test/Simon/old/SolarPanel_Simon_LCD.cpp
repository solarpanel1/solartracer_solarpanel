// Code funktioniert mit dieser Library:
// marcoschwartz/LiquidCrystal_I2C @ ^1.1.4

#include <Arduino.h>
#include <Wire.h>
#include "SPI.h"

#define HwSerial Serial4 // RS3485 Chip

// LEDs
#define LED4_GN 29 // Links unten
#define LED5_RD 30 // Links oben
#define LED6_YG 31 // Rechts unten
#define LED7_BL 32 // Rechts oben
#define LED_DE 41  // DE für RS3485

// Adafruit LCD 20x4
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C
    lcd(0x27, 20,
        4); // set the LCD address to 0x27 for a 16 chars and 2 line display

void setup() {
    // Init
    Serial.begin(115200);
    HwSerial.begin(115200);

    // LED init
    pinMode(LED4_GN, OUTPUT);
    pinMode(LED5_RD, OUTPUT);
    pinMode(LED6_YG, OUTPUT);
    pinMode(LED7_BL, OUTPUT);
    pinMode(LED_DE, OUTPUT);
    digitalWrite(LED4_GN, HIGH);
    digitalWrite(LED5_RD, HIGH);
    digitalWrite(LED6_YG, HIGH);
    digitalWrite(LED7_BL, HIGH);
    digitalWrite(LED_DE, HIGH);
    // wait for serial port to connect. Needed for native USB
    while(!Serial) { ; }

    digitalWrite(LED4_GN, LOW);
    digitalWrite(LED5_RD, LOW);
    digitalWrite(LED6_YG, LOW);
    digitalWrite(LED7_BL, LOW);
    digitalWrite(LED_DE, HIGH);

    // LCD 20x4 Adafruit
    lcd.init();
    delay(500);
    lcd.backlight();
    lcd.setCursor(0, 0);
    lcd.print("*------------------*");
    lcd.setCursor(0, 1);
    lcd.print("  Hallo");
    lcd.setCursor(0, 2);
    lcd.print("  Geert Boonstra  ");
    lcd.setCursor(0, 3);
    lcd.print("*------------------*");
    Serial.println("LCD Gestartet");
}

void loop() {}