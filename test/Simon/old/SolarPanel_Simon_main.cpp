#include <Arduino.h>
#include <Wire.h>
#include "SPI.h"

// LEDs
#define LED4_GN 29 // Links unten
#define LED5_RD 30 // Links oben
#define LED6_YG 31 // Rechts unten
#define LED7_BL 32 // Rechts oben
#define LED_DE 41  // DE für RS3485

// Adafruit RTC DS3231
// https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/arduino-usage
// https://github.com/adafruit/RTClib
#include "RTClib.h"
RTC_DS3231 rtc;
char daysOfTheWeek[7][12] = {"Sonntag",    "Montag",  "Dienstag", "Mittwoch",
                             "Donnerstag", "Freitag", "Samstag"};

void setup() {
    // Init
    Serial.begin(115200);
    while(!Serial) { ; }
    Serial.println("Starte RTC Uhr");

    if(!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        // abort();
    }

    // LED init
    pinMode(LED4_GN, OUTPUT);
    pinMode(LED5_RD, OUTPUT);
    pinMode(LED6_YG, OUTPUT);
    pinMode(LED7_BL, OUTPUT);
    pinMode(LED_DE, OUTPUT);
    digitalWrite(LED4_GN, HIGH);
    digitalWrite(LED5_RD, HIGH);
    digitalWrite(LED6_YG, HIGH);
    digitalWrite(LED7_BL, HIGH);
    digitalWrite(LED_DE, HIGH);

    // wait for serial port to connect
    delay(2000);
    digitalWrite(LED4_GN, LOW);
    digitalWrite(LED5_RD, LOW);
    digitalWrite(LED6_YG, LOW);
    digitalWrite(LED7_BL, LOW);
    digitalWrite(LED_DE, LOW);

    if(rtc.lostPower()) {
        Serial.println("RTC lost power, let's set the time!");
        // When time needs to be set on a new device, or after a power loss, the
        // following line sets the RTC to the date & time this sketch was
        // compiled
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
        // This line sets the RTC with an explicit date & time, for example to
        // set January 21, 2014 at 3am you would call: rtc.adjust(DateTime(2014,
        // 1, 21, 3, 0, 0));
    }

    // When time needs to be re-set on a previously configured device, the
    // following line sets the RTC to the date & time this sketch was compiled
    // rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    // rtc.adjust(DateTime(2014, 1, 21, 3, 0, 0));
}

void loop() {
    DateTime now = rtc.now();

    // Datum
    char DateBuffer[15];
    sprintf(DateBuffer, "%s_%02d.%02d.%04d%c", "date", now.day(), now.month(),
            now.year(), '\n');
    Serial.print(DateBuffer);

    // Zeit
    char TimeBuffer[13];
    sprintf(TimeBuffer, "%s_%02d:%02d:%02d%c", "time", now.hour(), now.minute(),
            now.second(), '\n');
    Serial.print(TimeBuffer);

    // Temperatur
    char TemperatureBuffer[11];
    sprintf(TemperatureBuffer, "%s_%0.1f°C%c", "temp", rtc.getTemperature(),
            '\n');
    Serial.print(TemperatureBuffer);
}