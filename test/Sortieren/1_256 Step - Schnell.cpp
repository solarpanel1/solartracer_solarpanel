#include <Arduino.h>
#include <TMCStepper.h>

#define pinEN     17  // Enable
#define pinCS     10  // Chip select
#define pinCLK16  33  // CLOCK 16
#define R_SENSE 0.11f  // Match to your driver
TMC5130Stepper driver = TMC5130Stepper(pinCS, R_SENSE);

void setup() {
    SPI.begin();
    pinMode(pinEN, OUTPUT);
    pinMode(pinCLK16, OUTPUT);
    digitalWrite(pinEN, LOW);
    digitalWrite(pinCLK16, LOW);

    Serial.begin(4608000);
    driver.begin();
    driver.defaults();
    driver.push();

    driver.GCONF(0);
    // CHOPCONF
    driver.toff(4);
    driver.hstrt(4);
    driver.hend(0);
    driver.tbl(2);
    driver.chm(0);
    
    driver.TPOWERDOWN(10);
    driver.en_pwm_mode(true);
    driver.TPWMTHRS(2000);
    // PWMCONF
    driver.pwm_autoscale(true);
    driver.pwm_freq(3);       // 2/1024
    driver.pwm_ampl(0);
    driver.pwm_grad(1);
    driver.freewheel(1);

    // driver.mres(256);
    driver.TCOOLTHRS(800);

    // RAMP parameters (Full Stepping) Langsam
    driver.A1(      5000);
    driver.V1(   2500000);
    driver.AMAX(    2000);
    driver.VMAX( 2800000);
    driver.DMAX(   20000);
    driver.D1(     10000);
    driver.VSTOP(     10);
    driver.microsteps(64);

    driver.rms_current(2600);
    driver.RAMPMODE(0);
    driver.XACTUAL(0);
    driver.XTARGET(0);
}

unsigned long previousMillis = 0;
const long interval = 750;

void loop() {
    unsigned long currentMillis = millis();
    int incomingByte = 0;
    driver.shaft();

    if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;
        Serial.println("");
        Serial.println(driver.rms_current());
        Serial.println(driver.pwm_freq());
    }
    
    if (Serial.available()) { // if there is data comming
        // String command = Serial.readStringUntil('\n'); // read string until meet newline character
        incomingByte = Serial.read();
        // say what you got:
        // Serial.print("I received: ");
        // Serial.println(incomingByte, DEC);

        switch (incomingByte) {
            case  48: // Taste "0" = RampMode 0
                driver.RAMPMODE(0);
                break;
            case  49: // Taste "1" = RampMode 1
                driver.RAMPMODE(1);
                break;
            case  50: // Taste "2" = RampMode 2
                driver.RAMPMODE(2);
                break;
            case  51: // Taste "3" = RampMode 3
                driver.RAMPMODE(3);
                break;
            case 101: // Taste "E" = Einschalten
                digitalWrite(pinEN, LOW);
                break;
            case  97: // Taste "A" = Ausschalten
                driver.XTARGET(driver.XACTUAL()+5000000);
                break;
            case 107: // Taste "K" = Kill
                digitalWrite(pinEN, HIGH);
                break;
            case 105: // Taste "I" = Init
                Serial.println(driver.pwm_freq());
                Serial.println(driver.version());
                
                break;
            case 114: // Taste "r" = Runde
                driver.XACTUAL(0);
                driver.XTARGET(10000000);    // Full Stepping Langsam
                break;
            case  82: // Taste "R" = Runde
                break;
            case 112: // Taste "p" = print
                break;
            case  43: // Taste + = Schneller
                break;
            case  45: // Taste - = Langsamer
                break;
            default:
                // Tue etwas, im Defaultfall
                // Dieser Fall ist optional
            break; // Wird nicht benötigt, wenn Statement(s) vorhanden sind
        }
    }
}