#include <Arduino.h>
#include <Wire.h>
/*
  ReadAnalogsoll

  Reads an analog input on pin 0, converts it to soll, and prints the result to
  the Serial Monitor. Graphical representation is available using Serial Plotter
  (Tools > Serial Plotter menu). Attach the center pin of a potentiometer to pin
  A0, and the outside pins to +5V and ground.

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/ReadAnalogsoll
*/
// #include <AFMotor.h>
// AF_DCMotor(1)   //set Motor ( 1 = M1 , 2 = M2 ))
int potiPin = A17; // potentiometer wiper
int evalPin = A16; // Rotationsgeber

#define DIR_A 37
#define PWM_A 36
#define BRK_A 35

//   Setup Channel A
//  pinMode(12, OUTPUT); //Initiates Motor Channel A pin
//  pinMode(9, OUTPUT);  //Initiates Brake Channel A pin

int val;
int var;
int sollW;
int potentioVal;
int soll;
int evalPos;
int evalationValue;
boolean motorOn = false;
boolean flagButtonPressed;
boolean firstCall = true;
boolean startProg = true;
boolean received  =false;

float rotation;
// ---------------   TimeOut  defs

unsigned long lastTime = 0; // the last time the output pin was toggled
unsigned long timeOut  = 10000; // the time to wait for the next move; increase if the output flickers




// ---------------   Debounce defs    ------------------------------
const int buttonPin = 32; // the number of the pushbutton pin

int ledState = HIGH;        // the current state of the output pin
int buttonState;            // the current reading from the input pin
int lastButtonState = HIGH; // the previous reading from the input pin

// the following variables are unsigned longs because the time, measured in
// milliseconds, will quickly become a bigger number than can be stored in an
// int.

unsigned long lastDebounceTime = 0; // the last time the output pin was toggled
unsigned long debounceDelay    = 50; // the debounce time; increase if the output flickers

// ------------------------------------------------------------------

//  ========================================================================
//  =================       Subroutines     ================================
//  ========================================================================

// ---------------------------------------------------------------------------

void buttonPressed() {

    // read the state of the switch into a local variable:
    int reading = digitalRead(buttonPin);

    Serial.print("Button State = ");
    Serial.println(reading);

    // check to see if you just pressed the button
    // (i.e. the input went from LOW to HIGH), and you've waited long enough
    // since the last press to ignore any noise:

    // If the switch changed, due to noise or pressing:

    if(reading != lastButtonState) {
        // reset the debouncing timer
        lastDebounceTime = millis();
    }

    if((millis() - lastDebounceTime) > debounceDelay) {
        // whatever the reading is at, it's been there for longer than the
        // debounce delay, so take it as the actual current state:

        // if the button state has changed:
        if(reading != buttonState) {
            buttonState = reading;
            flagButtonPressed = true;
            // only toggle the LED if the new button state is HIGH
            if(buttonState == HIGH) {
                ledState = !ledState;
            }
        }
    }

    // set the LED:
    digitalWrite(LED_BUILTIN, ledState);

    // save the reading. Next time through the loop, it'll be the
    // lastButtonState:
    lastButtonState = reading;
}
//  *********************************     END SUBROUTINES

//  *********************************************

// ------------------------------------------------------------------------------
void setup() {

    // initialize digital pin LED_BUILTIN as an output.
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(evalPin, INPUT);
    // setup debounce

    pinMode(buttonPin, INPUT_PULLUP);
    // pinMode(ledPin,   OUTPUT);
    digitalWrite(LED_BUILTIN, ledState); // set initial LED state

    // initialize serial communication

    Serial.begin(19200);

    // Setup actuatorController
    // Setup Channel A
    pinMode(DIR_A, OUTPUT); // Initiates Motor Channel A pin alt 12
    pinMode(BRK_A, OUTPUT); // Initiates Brake Channel A pin alt 9
    // pinMode(PWM_A, OUTPUT);
    flagButtonPressed = false;
    lastTime = millis();

}

//**********************************************************************
// -----------                   MAIN              --------------------
//***********************************************************************

void loop() {


    if ((millis() - lastTime) > timeOut) {
        //RUN Motor
        Serial.println("TIMEOUT  ==========================");
        lastTime = millis();

    }

    int incomingByte = 43;

    received = true;
    /*
        if(startProg) {
            buttonPressed();
            flagButtonPressed = false;
            startProg = false;
        }

        while (!buttonState) {
            buttonPressed();
            if (flagButtonPressed){
                flagButtonPressed = false;
            }
        }
    */
    evalationValue = analogRead(evalPin);
    evalPos = evalationValue;

    Serial.print("elevation = ");
    Serial.print(evalPos);

    potentioVal = analogRead(potiPin);
    soll = potentioVal;

    Serial.print("  soll = ");
    Serial.println(soll);

    if(Serial.available()) { // if there is data comming
        // String command = Serial.readStringUntil('\n'); // read string until
        // meet newline character
        incomingByte = Serial.read();
        // say what you got:
        Serial.print("I received: ");
        Serial.println(incomingByte, DEC);
    }
    else{
        if (received) {
        Serial.print("I did received: ");
        Serial.println(incomingByte, DEC);
    
        }
    }
    switch(incomingByte) {
    case 48: // Taste "0" = RampMode 0

        break;
    default:
        // Tue etwas, im Defaultfall
        // Dieser Fall ist optional
        break; // Wird nicht benötigt, wenn Statement(s) vorhanden sind
    }

    if(!motorOn) {

        if(flagButtonPressed) {
            flagButtonPressed = false;
            Serial.println("Button Pressed");
            delay(50);
            potentioVal = analogRead(potiPin);
            soll = potentioVal;

            evalationValue = analogRead(evalPin);
            evalPos = evalationValue;

            if(evalPos > 900) {
                evalPos = 0;
                Serial.print("elevationkorr = ");
                Serial.println(evalPos);
            }

            if(evalPos < soll) {
                var = 1;
                //            motorOn = true;
            } else if(evalPos > soll) {
                var = 2;
                //              motorOn = true;
            } else {
                var = 3;
            }
        } //  Ende flagButtonPressed

        else { //  d.H. button not pressed
            if(!firstCall) {
                buttonPressed();
            } else {
                firstCall = false;
            }
            Serial.println("Button Pressed 2");
            var = 3;
            delay(1000);
        }

    } // Ende !motorOn

    else {
        // Nothing to DO ?
    }

    var = 1;
    switch(var) {
    case 0:
        // do something when var equals 0
        break;
        // -----------------------------------------
    case 1:
        potentioVal = analogRead(potiPin);
        soll = potentioVal;

        evalationValue = analogRead(evalPin);
        evalPos = evalationValue;

        if(evalPos > 900) {
            evalPos = 0;
        }
        if(evalPos < soll) {
            Serial.println("Motor Case 1");

            if(!motorOn) {
                motorOn = true;
                digitalWrite(
                    DIR_A, HIGH); // Establishes forward direction of Channel A
                digitalWrite(BRK_A, LOW); // Disengage the Brake for Channel A
                analogWrite(PWM_A,
                            255); // Spins the motor on Channel A at half speed
            }
        } else {
            motorOn = false;
            digitalWrite(BRK_A, HIGH); // Engage the Brake for Channel A
            analogWrite(PWM_A, 0);     // Spins the motor on Channel A at stop
            flagButtonPressed = false;
        }
        break;
        // -----------------------------------------
    case 2:
        potentioVal = analogRead(potiPin);
        soll = potentioVal;

        evalationValue = analogRead(evalPin);
        evalPos = evalationValue;

        if(evalPos > 900) {
            evalPos = 0;
        }
        if(evalPos > soll) {
            Serial.println("Motor Case 2");
            motorOn = false;
            if(!motorOn) {
                motorOn = true;
                digitalWrite(
                    DIR_A, LOW); // Establishes backward direction of Channel A
                digitalWrite(BRK_A, LOW); // Disengage the Brake for Channel A
                analogWrite(PWM_A,
                            255); // Spins the motor on Channel A at half speed

            } else {
                motorOn = false;
                digitalWrite(BRK_A, HIGH); // Engage the Brake for Channel A
                analogWrite(PWM_A, 0); // Spins the motor on Channel A at stop
                flagButtonPressed = false;
            }
        }
        if(evalPos < 10) {
            analogWrite(PWM_A,
                        255); // Spins the motor on Channel A at half speed
        }

        break;
        // -----------------------------------------
    case 3:
        // do something when var equals 3
        break;
        // -----------------------------------------

    case 4:
        // do something when var equals 3
        //        buttonPressed();
        break;

    // -----------------------------------------
    default:
        // if nothing else matches, do the default
        // default is optional
        break;
    } // End switch !!

    //  if    ( motorOn) { delay(10);   }
    //  else             { delay(1000); }

} //  END MAIN
