/// Sender
// Funktioniert 22.08.2021 20:50 mit Standard teensy loader
#include <Arduino.h>
// Touchscreen
#include <Adafruit_GFX.h>
// #include <myFonts/font_Michroma.h>
// #include <ili9341_t3n_font2font_Michroma.h>
// #include "font_Arial.h"
// #include "font_ArialBold.h"
// #include <Fonts/Org_01.h>
// #include <Fonts/FreeSerif9pt7b.h>
// #include <fonts/FreeSans12pt7b.h>
// #include <Fonts/FreeMono9pt7b.h>
// #include <Fonts/FreeSans9pt7b.h>
// #include <ILI9341_fonts.h>
#include <Wire.h>
#include "SPI.h"
#include <ili9341_t3n_font_Arial.h>
#include <ili9341_t3n_font_ArialBold.h>
#include <ili9341_t3n_font_ComicSansMS.h>
#include <ili9341_t3n_font_OpenSans.h>
#include <myFonts/font_Michroma.h>
#include <myFonts/font_Michroma.c>
#include "ILI9341_t3n.h"

// #include <MCUFRIEND_kbv.h>
// MCUFRIEND_kbv tft;

#define TFT_SS 6
#define TFT_RDANDWR 7
#define TFT_DC 9
#define TFT_CS 10
#define TFT_RST 8
#define HwSerial Serial7
#define CENTER ILI9341_t3n::CENTER
DMAMEM uint16_t fb1[320 * 240];
DMAMEM uint16_t fb2[320 * 240];

ILI9341_t3n tft = ILI9341_t3n(TFT_CS, TFT_DC, TFT_RST);

// Adafruit RTC DS3231
#include "RTClib.h"
// https://www.adafruit.com/product/3013
RTC_DS3231 rtc;

char daysOfTheWeek[7][12] = {"Sonntag",    "Montag",  "Dienstag", "Mittwoch",
                             "Donnerstag", "Freitag", "Samstag"};

void waitUserInput() {
    Serial.println("Press any key to continue");
    while(Serial.read() == -1) {};
    while(Serial.read() != -1) {};
    Serial.println("Continued");
}

void setup() {
    pinMode(TFT_RST, OUTPUT);
    pinMode(TFT_SS, OUTPUT);
    pinMode(TFT_RDANDWR, OUTPUT);

    digitalWrite(TFT_RST, HIGH);
    digitalWrite(TFT_SS, HIGH);
    digitalWrite(TFT_RDANDWR, LOW);

    Serial.begin(115200);
    HwSerial.begin(115200);
    Serial.println("Serial Begin");

    while(!Serial) {};
    if(!rtc.begin()) {
        Serial.println("Couldn't find RTC");
        Serial.flush();
        abort();
    }
    Serial.println("Started");

    if(rtc.lostPower()) {
        Serial.println("RTC lost power, let's set the time!");
        rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    }
    //  rtc.adjust(DateTime(F("Jul 27 2021"),F("23:32:01")));
    // Serial.println("Zeit gesetzt");

    tft.begin();
    tft.setRotation(1);
    tft.setTextSize(2);

    Serial.println("Example: Font_simple");
    Serial.print("found ID = 0x");

    tft.setFont(Michroma_12);
    tft.setFrameBuffer(fb1);
    // http://www.barth-dev.de/online/rgb565-color-picker/
    tft.fillScreen(0xFFE0);
}

void loop() {
    DateTime now = rtc.now();

    // Datum
    char DateBuffer[15];
    sprintf(DateBuffer, "%s_%02d.%02d.%04d%c", "date", now.day(), now.month(),
            now.year(), '\n');
    // HwSerial.print(DateBuffer);
    HwSerial.print(DateBuffer);
    Serial.print(DateBuffer);

    // Zeit
    char TimeBuffer[13];
    sprintf(TimeBuffer, "%s_%02d:%02d:%02d%c", "time", now.hour(), now.minute(),
            now.second(), '\n');
    HwSerial.print(TimeBuffer);
    Serial.print(TimeBuffer);

    // Temperatur
    char TemperatureBuffer[11];
    sprintf(TemperatureBuffer, "%s_%0.1f°C%c", "temp", rtc.getTemperature(),
            '\n');
    HwSerial.print(TemperatureBuffer);
    Serial.print(TemperatureBuffer);
}