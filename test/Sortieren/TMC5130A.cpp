#include <Arduino.h>
#include <TMCStepper.h>

/**
 * @brief TestATestATestATestATestATestA
 *
 */
#define pinEN 17    ///< Enable pin
#define pinCS 10    ///< Chip select pin
#define pinCLK16 33 ///< CLOCK 16 pin
#define R_SENSE 0.11f

int STALL_VALUE = -63;           // [-64..63]
uint32_t my_ihold_irun = 0x1f00; // Set IRUN to 31 (DEC) and IHOLD to 31 (DEC)
uint8_t my_FreeWheel = 0x1;
uint8_t myGrad = 0x80;

TMC5130Stepper driver = TMC5130Stepper(pinCS, R_SENSE);
void setup() {
    SPI.begin();
    pinMode(pinEN, OUTPUT);
    pinMode(pinCLK16, OUTPUT);
    digitalWrite(pinEN, LOW);
    digitalWrite(pinCLK16, LOW);

    Serial.begin(4608000);
    driver.begin();
    driver.defaults();
    driver.push();
    driver.GCONF(0);

    // ** CHOPCONF **
    driver.toff(4);
    driver.blank_time(24);
    driver.hstrt(4);
    driver.hend(0);
    driver.tbl(2);
    driver.chm(0);
    // ** ??? **
    driver.TPOWERDOWN(10);
    // ** ??? **
    driver.TPWMTHRS(2000);
    driver.TZEROWAIT(512);
    // ** PWMCONF **

    // driver.ihold(0b00000000);
    // driver.irun(0b00010000);
    // driver.iholddelay(0b00000000);

    driver.ihold(0);
    driver.irun(16);
    driver.iholddelay(0);

    // driver.IHOLD_IRUN(my_ihold_irun);

    driver.en_pwm_mode(true); // Toggle stealthChop on TMC2130/2160/5130/5160
    driver.pwm_ampl(0);
    driver.pwm_grad(1);
    driver.pwm_freq(0);         // 2/1024
    driver.pwm_autoscale(true); // Needed for stealthChop
    driver.pwm_symmetric(0);
    driver.freewheel(1);

    // ** Stallguard **

    // ** RAMP parameters (Full Stepping) Langsam **
    driver.A1(6000);
    driver.V1(400000);
    driver.AMAX(500);
    // driver.VMAX(  450000);
    driver.VMAX(600000);
    driver.DMAX(8000);
    driver.D1(5000);
    driver.VSTOP(10);
    driver.microsteps(16);
    // ** Current **
    driver.rms_current(1500);

    // driver.mres(256);
    // ** COOLCONF **
    // driver.COOLCONF(1); // Selber

    driver.sfilt(true); // Beispiel TMC2660 / Improves SG readout.
    driver.seimin(5);   // Selber
    driver.semin(2);    // Beispiel TMC2660
    driver.semax(4);    // Beispiel TMC2660
    driver.seup(2);
    driver.sedn(2);
    driver.TCOOLTHRS(12000);
    driver.THIGH(500000);
    driver.sedn(0b01);       // Beispiel TMC2660
    driver.sgt(STALL_VALUE); // Beispiel TMC2660

    // ** Positioning **
    driver.RAMPMODE(0); // 0 = To Position, 1 = Constant velocity
    driver.XACTUAL(0);
    driver.XTARGET(0);
}

unsigned long previousMillis = 0;
const long interval = 750;
bool toggle = false; ///< Schaltet die Ausgabe der Variablen im Serialmonitor
                     ///< ein oder aus

void loop() {
    unsigned long currentMillis = millis();
    int incomingByte = 0;

    driver.shaft();

    if(toggle == true) {
        if(currentMillis - previousMillis >= interval) {
            // Mit Taste "t" wie toggle ein/ausschalten
            // Standard = aus
            previousMillis = currentMillis;
            Serial.print("vzero:");
            Serial.print(driver.vzero());
            Serial.print(" CS_saActual: ");
            Serial.print(driver.cs_actual());
            Serial.print(" RMS Current: ");
            Serial.print(driver.rms_current());
            Serial.print(" XACTUAL: ");
            Serial.print(driver.XACTUAL());
            Serial.print(" XTARGET: ");
            Serial.print(driver.XTARGET());
            Serial.print(" GSTAT: ");
            Serial.print(driver.GSTAT());
            Serial.print(" Over Temp. Prewarn: ");
            Serial.print(driver.otpw());
            Serial.print(" Over Temp: ");
            Serial.println(driver.otpw());
        }
    }

    if(Serial.available()) { // if there is data comming
        // String command = Serial.readStringUntil('\n'); // read string
        // until meet newline character
        incomingByte = Serial.read();
        // say what you got:
        Serial.print("I received: ");
        Serial.println(incomingByte, DEC);

        switch(incomingByte) {
        case 48: // Taste "0" = RampMode 0
            driver.RAMPMODE(0);
            break;
        case 49: // Taste "1" = RampMode 1
            driver.RAMPMODE(1);
            break;
        case 50: // Taste "2" = RampMode 2
            driver.RAMPMODE(2);
            break;
        case 51: // Taste "3" = RampMode 3
            driver.RAMPMODE(3);
            break;
        case 97: // Taste "a"
            break;
        case 115: // Taste "s" = Stop
            driver.RAMPMODE(1);
            driver.VMAX(0);
            driver.AMAX(8000);
            break;
        case 114: // Taste "r" = Run
            driver.AMAX(500);
            driver.VMAX(6000);
            driver.RAMPMODE(0);
            driver.XACTUAL(0);
            driver.XTARGET(2592000);
            break;
        case 101: // Taste "e" = Einschalten
            driver.AMAX(500);
            driver.VMAX(600000);
            driver.RAMPMODE(0);
            driver.XACTUAL(0);
            digitalWrite(pinEN, LOW);
            break;
        case 116: // Taste "t" = toggle
            if(toggle == true) {
                toggle = false;
            } else {
                toggle = true;
            }
            break;
        case 107: // Taste "k" = Kill
            if(driver.vzero() == 0) {
                digitalWrite(pinEN, HIGH);
            }
            break;
        case 105: // Taste "info" = Init
            // READ_RDSEL10_t data{0};
            // data.sr = driver.DRV_STATUS();

            // Serial.print("0 ");
            // Serial.print(data.sg_result, DEC);
            // Serial.print(" ");
            // Serial.println(driver.cs2rms(data.se), DEC);
            break;
        case 112: // Taste "p" = print
            break;
        case 43: // Taste + = Schneller
            driver.VMAX(driver.VMAX() + 2000);
            break;
        case 45: // Taste - = Langsamer
            driver.VMAX(driver.VMAX() - 2000);
            break;
        default:
            // Tue etwas, im Defaultfall
            // Dieser Fall ist optional
            break; // Wird nicht benötigt, wenn Statement(s)
                   // vorhanden sind
        }
    }
}