#include <Arduino.h>
#include <TMCStepper.h>

#define pinEN 17     // Enable
#define pinCS 10     // Chip select
#define pinCLK16 33  // CLOCK 16
#define R_SENSE 0.11f

int STALL_VALUE = -63;  // [-64..63]

TMC5130Stepper driver = TMC5130Stepper(pinCS, R_SENSE);

void setup() {
    SPI.begin();
    pinMode(pinEN, OUTPUT);
    pinMode(pinCLK16, OUTPUT);
    digitalWrite(pinEN, LOW);
    digitalWrite(pinCLK16, LOW);
    driver.IHOLD_IRUN(0x1001);

    Serial.begin(4608000);
    driver.begin();
    driver.defaults();
    driver.push();

    driver.GCONF(0);

    // ** CHOPCONF **
    driver.toff(4);
    driver.blank_time(24);
    driver.hstrt(4);
    driver.hend(0);
    driver.tbl(2);
    driver.chm(0);
    // ** ??? **
    driver.TPOWERDOWN(10);
    // ** ??? **
    driver.TPWMTHRS(2000);
    driver.TZEROWAIT(512);
    // ** PWMCONF **
    driver.pwm_autoscale(true);
    driver.en_pwm_mode(true);
    driver.pwm_freq(3);  // 2/1024
    driver.pwm_ampl(0);
    driver.pwm_grad(1);
    driver.freewheel(1);
    // ** Stallguard **

    // ** RAMP parameters (Full Stepping) Langsam **
    // RAMP parameters (Full Stepping) Langsam
    driver.A1(50);
    driver.V1(500);
    driver.AMAX(5);
    driver.VMAX(10000);
    driver.DMAX(100);
    driver.D1(300);
    driver.VSTOP(10);
    driver.microsteps(4);
    // ** Current **
    driver.rms_current(4000);

    // driver.mres(256);
    // ** COOLCONF **
    // driver.COOLCONF(1); // Selber

    driver.sfilt(true);  // Beispiel TMC2660 / Improves SG readout.
    driver.seimin(5);    // Selber
    driver.semin(2);     // Beispiel TMC2660
    driver.semax(4);     // Beispiel TMC2660
    driver.seup(2);
    driver.sedn(2);
    driver.TCOOLTHRS(12000);
    driver.THIGH(500000);
    driver.sedn(0b01);        // Beispiel TMC2660
    driver.sgt(STALL_VALUE);  // Beispiel TMC2660

    // ** Positioning **
    driver.RAMPMODE(0);  // 0 = To Position, 1 = Constant velocity
    driver.XACTUAL(0);
    driver.XTARGET(0);
}

unsigned long previousMillis = 0;
const long interval = 750;
bool toggle = 0;

void loop() {
    unsigned long currentMillis = millis();
    int incomingByte = 0;

    // driver.shaft();

    if (toggle == true) {
        if (currentMillis - previousMillis >= interval) {
            // Mit Taste "t" wie toggle ein/ausschalten
            // Standard = aus
            previousMillis = currentMillis;
            // Serial.print("vzero:");
            // Serial.println(driver.vzero());
            // Serial.print(" CS aActual: ");
            // Serial.print(driver.cs_actual());
            // Serial.print(" RMS Current: ");
            // Serial.print(driver.rms_current());
            Serial.print(" XACTUAL: ");
            Serial.println(driver.XACTUAL());
            // Serial.print(" XTARGET: ");
            // Serial.print(driver.XTARGET());
            // Serial.print(" GSTAT: ");
            // Serial.print(driver.GSTAT());
            // Serial.print(" Over Temp. Prewarn: ");
            // Serial.print(driver.otpw());
            // Serial.print(" Over Temp: ");
            // Serial.println(driver.otpw());
            // Serial.print(" IHOLD_IRUN: ");
            // Serial.println(driver.IHOLD_IRUN());
        }
    }

    if (Serial.available()) {  // if there is data comming
        // String command = Serial.readStringUntil('\n'); // read string until
        // meet newline character
        incomingByte = Serial.read();
        // say what you got:
        Serial.print("I received: ");
        Serial.println(incomingByte, DEC);

        switch (incomingByte) {
            case 48:  // Taste "0" = RampMode 0
                driver.RAMPMODE(0);
                break;
            case 49:  // Taste "1" = RampMode 1
                driver.RAMPMODE(1);
                break;
            case 50:  // Taste "2" = RampMode 2
                driver.RAMPMODE(2);
                break;
            case 51:  // Taste "3" = RampMode 3
                driver.RAMPMODE(3);
                break;
            case 97:  // Taste "a"
                break;
            case 115:  // Taste "s" = Stop
                driver.RAMPMODE(1);
                driver.VMAX(0);
                driver.AMAX(8000);
                break;
            case 114:  // Taste "r" = Runde
                driver.AMAX(500);
                driver.VMAX(2000);
                driver.RAMPMODE(0);
                driver.XACTUAL(0);
                driver.shaft(1);
                driver.XTARGET(12500);
                break;
            case 101:  // Taste "e" = Einschalten
                driver.AMAX(500);
                driver.VMAX(60000);
                driver.RAMPMODE(0);
                driver.XACTUAL(400000);
                digitalWrite(pinEN, LOW);
                break;
            case 116:  // Taste "t" = toggle
                if (toggle == true) {
                    toggle = false;
                } else {
                    toggle = true;
                }
                break;
            case 107:  // Taste "k" = Kill
                if (driver.vzero() == 0) {
                    digitalWrite(pinEN, HIGH);
                }
                break;
            case 105:  // Taste "info" = Init
                       // READ_RDSEL10_t data{0};
                       // data.sr = driver.DRV_STATUS();

                // Serial.print("0 ");
                // Serial.print(data.sg_result, DEC);
                // Serial.print(" ");
                // Serial.println(driver.cs2rms(data.se), DEC);
                break;
            case 112:  // Taste "p" = print
                break;
            case 43:  // Taste + = Schneller
                driver.VMAX(driver.VMAX() + 2000);
                break;
            case 45:  // Taste - = Langsamer
                driver.VMAX(driver.VMAX() - 2000);
                break;
            default:
                // Tue etwas, im Defaultfall
                // Dieser Fall ist optional
                break;  // Wird nicht benötigt, wenn Statement(s) vorhanden sind
        }
    }
}